import { COLORS, LABELS, TIMETOTABLEVALUES } from '../constants';
import chaiColors from 'chai-colors'
chai.use(chaiColors);


const assertHomemadeIconShouldBeVisible = () => {
  return (
    cy.get(`[data-test-id=${LABELS.HOME}]`)
      .should('be.visible')
  );
};

const assertDeliveryIconShouldBeVisible = () => {
  return (
    cy.get(`[data-test-id=${LABELS.DELIVERY}]`)
      .should('be.visible')
  );
};

const clickHomemadeFilterButton = () => {
  return (
    cy.get('[data-test-id=home-filter-button]')
      .click()
  );
};

const clickDeliveryFilterButton = () => {
  return (
    cy.get('[data-test-id=delivery-filter-button]')
      .click()
  );
};

describe('FoodPick app', function () {

  beforeEach(function () {
    cy.request('POST', './api/testing/reset');
    cy.visit('/');

    const user = {
      name: 'Bubbola de Bubbis',
      username: 'bubba',
      password: 'bubba'
    };
    cy.request('POST', './api/users', user);
    cy.visit('/');
  });

  describe('when landing on the homepage', function () {

    it('shows information about the app and a demo button', function () {
      cy.contains('How it works', { matchCase: false });
      cy.contains(LABELS.BUTTONS.DEMO);
    });

    it('shows the login form by clicking on the demo button', function () {
      cy.contains(LABELS.BUTTONS.DEMO)
        .click();
      cy.contains('username');
      cy.contains('password');
    });

    it('shows the login form by clicking on the login link', function () {
      cy.contains(LABELS.BUTTONS.LOGIN)
        .click();
      cy.contains('username');
      cy.contains('password');
    });

  });

  describe('login', function () {
    beforeEach(function () {
      cy.contains(LABELS.BUTTONS.LOGIN)
        .click();
    });

    it('succeeds with valid credentials', function () {
      cy.get('[data-test-id=username]')
        .type('bubba');
      cy.get('[data-test-id=password]')
        .type('bubba');
      cy.get('[data-test-id=login-button]')
        .click();

      cy.get('.alert-success')
        .should('contain', 'Welcome');

      cy.get('[data-test-id=nav-dropdown-user]')
        .should('contain', 'bubba');

      cy.get('html')
        .should('not.contain', LABELS.BUTTONS.LOGOUT);
    });

    it('fails and notifies if password is wrong', function () {
      cy.get('[data-test-id=username]')
        .type('bubba');
      cy.get('[data-test-id=password]')
        .type('wrong password');
      cy.get('[data-test-id=login-button]')
        .click();

      cy.get('.alert-danger')
        .contains('invalid password', { matchCase: false });
      cy.get('html')
        .should('not.contain', LABELS.BUTTONS.LOGOUT);
    });

    it('prevents from submitting if password is missing', function () {
      cy.get('[data-test-id=username]')
        .type('bubba');
      cy.get('[data-test-id=login-button]')
        .click();

      cy.get('html')
        .should('not.contain', LABELS.BUTTONS.LOGOUT);
    });

    it('prevents from submitting if username is missing', function () {
      cy.get('[data-test-id=password]')
        .type('bubba');
      cy.get('[data-test-id=login-button]')
        .click();

      cy.get('html')
        .should('not.contain', LABELS.BUTTONS.LOGOUT);
    });
  });

  describe('when logged in', function () {
    beforeEach(function () {
      cy.login({
        username: 'bubba',
        password: 'bubba'
      });
    });

    it('shows a search bar with a button', function () {
      cy.get('[data-test-id=unibar]');
      cy.contains(LABELS.BUTTONS.ADD);
      cy.contains(LABELS.EMPTYMEALLIST);
    });

    it('opens the meal form by pressing enter', function () {
      cy.get('[data-test-id=unibar]')
        .type('new meal')
        .type('{enter}');
      cy.get('.card-body')
        .should('be.visible');
    });

    it('opens the meal form by clicking the button', function () {
      cy.get('[data-test-id=unibar]')
        .type('new meal');
      cy.contains(LABELS.BUTTONS.ADD)
        .click();
      cy.get('[data-test-id=meal-form]')
        .should('be.visible');
    });

    it('can toggle between showing filters panel and meal form', function () {
      cy.contains(LABELS.BUTTONS.ADD)
        .click();
      cy.get('[data-test-id=meal-form]')
        .should('be.visible');

      cy.contains(LABELS.BUTTONS.FILTERS)
        .click();
      cy.get('[data-test-id=filter-container]')
        .should('be.visible');
    });

    describe('when creating a new meal', function () {

      it('creates one and only one meal', function () {
        cy.get('[data-test-id=unibar]')
          .type('new meal')
          .type('{enter}');
        cy.contains(LABELS.BUTTONS.SAVE)
          .click();

        cy.get('[data-test-id=meal-list]')
          .children()
          .should('have.length', 1);
      });

      it('saves a meal with just the meal name', function () {
        cy.get('[data-test-id=unibar]')
          .type('new meal')
          .type('{enter}');
        cy.contains(LABELS.BUTTONS.SAVE)
          .click();

        cy.get('.alert-success');
        cy.contains('new meal');
        cy.get('.card-body')
          .should('not.contain', 'time');
        cy.get('.card-body')
          .should('not.contain', 'home');
        cy.get('.card-body')
          .should('not.contain', 'delivery');
      });

      it('saves a meal with meal name, time, home and ingredients', function () {
        cy.get('[data-test-id=unibar]')
          .type('new meal')
          .type('{enter}');
        cy.get('[data-test-id=home-form-button]')
          .click();
        cy.contains(LABELS.TIME)
          .get('select')
          .select(TIMETOTABLEVALUES[0].toString());
        cy.get('[data-test-id=ingredients]')
          .type('ingredient')
          .type('{enter}');
        cy.contains(LABELS.BUTTONS.SAVE)
          .click();

        cy.get('.alert-success')
          .should('be.visible');
        cy.get('.card-body')
          .should('contain', 'new meal');
        cy.get('[data-test-id=meal-card]')
          .should('contain', `${TIMETOTABLEVALUES[0].toString()} ${LABELS.TIMESPEC}`);
        assertHomemadeIconShouldBeVisible();
        cy.get('[data-test-id=meal-card]')
          .should('contain', 'ingredient');
      });

      it('saves a meal with meal name and delivery', function () {
        cy.get('[data-test-id=unibar]')
          .type('new meal')
          .type('{enter}');
        cy.get('[data-test-id=delivery-form-button]')
          .click();
        cy.contains(LABELS.BUTTONS.SAVE)
          .click();

        cy.get('.alert-success')
          .should('be.visible');
        cy.get('.card-body')
          .should('contain', 'new meal');
        assertDeliveryIconShouldBeVisible();
      });
    });

    it('lets the user log out', function () {
      cy.get('[data-test-id=nav-dropdown-user]')
        .click();

      cy.contains(LABELS.BUTTONS.LOGOUT)
        .click();

      cy.contains(LABELS.BUTTONS.LOGIN);
    });

    describe('when some meals are saved', function () {
      beforeEach(function () {
        cy.createMeal({
          name: 'meal_1',
          time: '60',
          home: true,
          ingredients: ['ingredient 1', 'ingredient 2', 'ingredient 3']
        });
        cy.createMeal({
          name: 'meal_2',
          time: '20',
          home: false,
        });
        cy.createMeal({
          name: 'meal_3',
          time: '60',
          home: true,
          ingredients: ['ingredient 2', 'ingredient 3']
        });
      });

      it('shows the user\'s meals and a search bar', function () {
        cy.contains('meal_1');
        cy.contains('meal_2');
        cy.contains('meal_3');
        cy.get('[data-test-id=unibar]');
      });

      it('lets the user delete a meal', function () {
        cy.contains('meal_2')
          .get('[data-test-id=delete-button-meal_2]')
          .click();

        cy.get('.alert-success');
        cy.contains('meal_1');
        cy.should('not.contain', 'meal_2');
      });

      describe('when searching a meal', function () {

        it('lets the user find a meal by meal name', function () {
          cy.get('[data-test-id=unibar]')
            .type('1');

          cy.get('[data-test-id=meal-list]')
            .contains('meal_1')
            .should('not.contain', 'meal_2');
        });

        it('shows no meal if the search has no match', function () {
          cy.get('[data-test-id=unibar]')
            .type('z');

          cy.get('[data-test-id=user-page]')
            .should('not.contain', 'meal_1')
            .should('not.contain', 'meal_2');
        });

        it('shows filter options by clicking a button', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();

          assertHomemadeIconShouldBeVisible();
          assertDeliveryIconShouldBeVisible();
          cy.get('[data-test-id=filter-container]')
            .contains(LABELS.INGREDIENTS);
        });

        it('clear filters by clicking clear button', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();
          clickHomemadeFilterButton();
          cy.contains(LABELS.INGREDIENTS)
            .click();

          cy.get('[data-test-id=filter-container]')
            .contains('ingredient 1')
            .click();

          cy.get('[data-test-id=meal-list]')
            .should('contain', 'meal_1')
            .should('not.contain', 'meal_3');

          cy.contains(LABELS.BUTTONS.CLEAR)
            .click();

          cy.get('[data-test-id=meal-list]')
            .should('contain', 'meal_1')
            .should('contain', 'meal_2')
            .should('contain', 'meal_3');

        });

        it('hides delivery meals by clicking home button', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();
          clickHomemadeFilterButton();

          cy.get('[data-test-id=meal-list]')
            .children('[data-test-id=meal-card]')
            .should('have.length', 2);
        });

        it('hides homemade meals by clicking delivery button', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();
          clickDeliveryFilterButton();

          cy.get('[data-test-id=meal-list]')
            .children('[data-test-id=meal-card]')
            .should('have.length', 1);
        });

        it('can toggle ingredients filter panel by clicking ingredients filter button', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();
          cy.get('[data-test-id=filter-container]')
            .contains(LABELS.INGREDIENTS)
            .click();

          cy.get('[data-test-id=filter-container]')
            .should('contain', 'ingredient 1');

          cy.get('[data-test-id=filter-container]')
            .contains(LABELS.INGREDIENTS)
            .click();

          cy.get('[data-test-id=filter-container]')
            .should('not.contain', 'ingredient 1');
        });

        it('filters meals by one ingredient by clicking the ingredient tag', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();
          cy.get('[data-test-id=filter-container]')
            .contains(LABELS.INGREDIENTS)
            .click();

          cy.get('[data-test-id=filter-container]')
            .contains('ingredient 1')
            .click();

          cy.get('[data-test-id=meal-list]')
            .should('contain', 'meal_1')
            .should('not.contain', 'meal_3');
        });

        it('filters meals by multiple ingredients by clicking multiple ingredient tags', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();
          cy.get('[data-test-id=filter-container]')
            .contains(LABELS.INGREDIENTS)
            .click();

          cy.get('[data-test-id=filter-container]')
            .contains('ingredient 2')
            .click();
          cy.get('[data-test-id=filter-container]')
            .contains('ingredient 1')
            .click();

          cy.get('[data-test-id=meal-list]')
            .should('contain', 'meal_1')
            .should('not.contain', 'meal_3');
        });

        it('shows clearly which filters are currently selected', function () {
          cy.contains(LABELS.BUTTONS.FILTERS)
            .click();

          cy.get('[data-test-id=home-filter-button]')
            .should('have.css', 'background-color', 'rgb(255, 255, 255)')

          clickHomemadeFilterButton();
          cy.contains(LABELS.INGREDIENTS)
            .click();
          cy.get('[data-test-id=filter-container]')
            .contains('ingredient 1')
            .click();

          cy.get('[data-test-id=home-filter-button]')
            .should('have.css', 'background-color')
            .and('be.colored', `${COLORS.SELECTEDFILTER}`)
          cy.get('[data-test-id=filter-container]')
            .contains('ingredient 1')
            .should('have.css', 'background-color')
            .and('be.colored', `${COLORS.SELECTEDFILTER}`)
        })
      });
    });
  });
});