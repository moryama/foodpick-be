const jwt = require('jsonwebtoken');
const User = require('../models/user');

const PASSWORD_MIN_LENGTH = 3;

// Separate token from authorization header
const getTokenFrom = (request) => {
  const authorization = request.get('authorization');

  if (authorization && authorization.toLowerCase().startsWith('bearer')) {
    const token = authorization.substring(7)
    return token;
  }

  return null;
};

const authenticateUser = async (request, response) => {

  const token = getTokenFrom(request);

  if (!token) {
    return response.status(401).json({ error: 'token is missing' });
  }

  const decodedToken = jwt.verify(token, process.env.SECRET);

  if (!decodedToken.id) {
    return response.status(401).json({ error: 'token invalid' });
  }

  const user = await User.findById(decodedToken.id);

  if (!user) {
    return response.status(401).json({ error: 'invalid user' });
  }

  return user;
};


module.exports = {
  PASSWORD_MIN_LENGTH,
  authenticateUser
};
