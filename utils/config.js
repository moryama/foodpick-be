require('dotenv').config();

const PORT = process.env.PORT;

let MONGODB_URI = process.env.MONGODB_URI;
const MONGO_USER = process.env.MONGO_USER;
const MONGO_PASS = process.env.MONGO_PASS;

if (process.env.NODE_ENV === 'test') {
  MONGODB_URI = process.env.TEST_MONGODB_URI;
}

module.exports = {
  MONGODB_URI,
  MONGO_USER,
  MONGO_PASS,
  PORT
};