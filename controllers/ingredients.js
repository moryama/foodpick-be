const ingredientsRouter = require('express').Router();
const Ingredient = require('../models/ingredient');
const removeDuplicateObjectsById = require('./helpers/removeDuplicateObjectsById');

ingredientsRouter.get('/:ingredientId', async (request, response) => {
  const ingredient = await Ingredient.findById(request.params.ingredientId);

  response.json(ingredient);
});

ingredientsRouter.get('/', async (request, response) => {
  const allIngredients = await Ingredient.find({})
  const uniqueIngredients = removeDuplicateObjectsById(allIngredients)

  response.json(uniqueIngredients)
})

module.exports = ingredientsRouter;