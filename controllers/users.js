const bcrypt = require('bcrypt');
const usersRouter = require('express').Router();
const User = require('../models/user');
const helper = require('../utils/auth_helper');


usersRouter.get('/', async (request, response) => {
  const users = await User
    .find({})
  response.json(users);
});

usersRouter.get('/:id', async (request, response) => {
  const user = await User.findById(request.params.id);
  user
    ? response.json(user)
    : response.status(404).json({
      error: 'User does not exist'
    });
});

usersRouter.post('/', async (request, response) => {
  const body = request.body;

  if (!body.username || !body.password) {
    return response.status(400).json({
      error: 'Username or password missing'
    });
  }
  if (body.password.length < helper.PASSWORD_MIN_LENGTH) {
    return response.status(400).json({
      error: 'Password must be minimum 3 characters'
    });
  }

  const saltRounds = 10;
  const passwordHash = await bcrypt.hash(body.password, saltRounds);

  const user = new User({
    username: body.username,
    passwordHash: passwordHash
  });

  await user.save();
  response.status(201).json(user);
});

usersRouter.delete('/:id', async (request, response) => {
  const userToDelete = await User.findByIdAndRemove(request.params.id);
  if (!userToDelete) {
    return response.status(404).json({
      error: 'User not found'
    });
  }

  response.status(204).end();
});

module.exports = usersRouter;