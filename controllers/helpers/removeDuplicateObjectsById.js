const removeDuplicateObjectsById = (arrayOfObjects) => {
  const seen = new Set();
  let uniqueObjects = [];

  arrayOfObjects.filter((item) => {
    if (seen.has(item.id)) {
      return false;
    }
    seen.add(item.id);
    uniqueObjects = [...uniqueObjects, item];
    return true;
  });

  return uniqueObjects;
};

module.exports = removeDuplicateObjectsById;
