

const getMealUniqueIngredients = (meal) => {
  let mealUniqueIngredients = []

  meal.ingredients.forEach(ingr => {
    const ingredientIdAsString = ingr.toString();

    if (mealUniqueIngredients.includes(ingredientIdAsString)) {
      return
    } else {
      mealUniqueIngredients.push(ingredientIdAsString)
    }
  })

  
  return mealUniqueIngredients
}

module.exports = getMealUniqueIngredients;