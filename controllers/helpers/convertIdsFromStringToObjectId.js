const mongoose = require('mongoose');

const convertIdsFromStringToObjectId = (idsAsString) => {
  const idsAsObjectId = idsAsString.map(ingrId => mongoose.Types.ObjectId(ingrId))
  return idsAsObjectId
}

module.exports = convertIdsFromStringToObjectId;