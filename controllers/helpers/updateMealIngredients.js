const Meal = require('../../models/meal');

const updateMealIngredients = async (meal, mealUniqueIngredients) => {

  const res = await Meal.updateOne({ _id: meal._id }, { ingredients: mealUniqueIngredients })

  console.log(meal.name + meal._id);
  if (res.acknowledged) {
    console.log('ALL WENT SMOOTHLY');
    console.log(res);
  } else {
    console.log('PROBLEM UPDATING DOCUMENT:', meal.name + meal.id);
  }
}

module.exports = updateMealIngredients;