const getMealUniqueIngredients = require("./getMealUniqueIngredients");

describe('getMealUniqueIngredients', () => {
  it('should return unique ingredients of the input meal', () => {
    const input = {
      id: '63bd38cb66708602c5e49bbe',
      ingredients: [
        "63bd38181780538396f4aef7",
        "63bd38ab1780538396f6102d",
        "63bd38ab1780538396f6102d"  // duplicate <==
      ],
      name: 'nostrud',
    }
    const expectedOutput = [
      "63bd38181780538396f4aef7",
      "63bd38ab1780538396f6102d",
    ]

    const output = getMealUniqueIngredients(input);

    expect(output).toEqual(expectedOutput);
  });

  it('should work if no duplicates are found', () => {
    const input = {
      id: '63bd38cb66708602c5e49bbe',
      ingredients: [
        "63bd38181780538396f4aef7",
        "63bd38ab1780538396f6102d",
      ],
      name: 'nostrud',
    }
    const expectedOutput = [
      "63bd38181780538396f4aef7",
      "63bd38ab1780538396f6102d",
    ]

    const output = getMealUniqueIngredients(input);

    expect(output).toEqual(expectedOutput);
  });
});
