const testingRouter = require('express').Router();
const Ingredient = require('../models/ingredient');
const Meal = require('../models/meal');
const User = require('../models/user');

testingRouter.post('/reset', async (request, response) => {
  await User.deleteMany({});
  await Meal.deleteMany({});
  await Ingredient.deleteMany({});

  response.status(204).end();
});

module.exports = testingRouter;