const mealsRouter = require('express').Router();
const Ingredient = require('../models/ingredient');
const Meal = require('../models/meal');
const helper = require('../utils/auth_helper');
const updateMealIngredients = require('./helpers/updateMealIngredients');
const getMealUniqueIngredients = require('./helpers/getMealUniqueIngredients');
const convertIdsFromStringToObjectId = require('./helpers/convertIdsFromStringToObjectId');

mealsRouter.get('/', async (request, response) => {

  const user = await helper.authenticateUser(request, response);

  const meals = await Meal
    .find({ user: user._id })
    .populate(('ingredients'))

  response.json(meals);
  return
});

/**
 * This endpoint should serve una tantum.
 * We'll keep it here anyway, until we won't.
 */
mealsRouter.get('/cleanUpDuplicateIngredients', async (request, response) => {
  const allMealsInDb = await Meal.find({})

  allMealsInDb.forEach(async (meal) => {
    const mealUniqueIngredients = getMealUniqueIngredients(meal)
    const hasDuplicates = mealUniqueIngredients.length < meal.ingredients.length
    const mealUniqueIngredientsAsObjectId = convertIdsFromStringToObjectId(mealUniqueIngredients)

    if (hasDuplicates) {
      updateMealIngredients(meal, mealUniqueIngredientsAsObjectId)
    } else {
      return
    }
  })

  return
})

mealsRouter.post('/', async (request, response) => {
  const body = request.body;

  const user = await helper.authenticateUser(request, response);

  let ingredientIds;
  if (request.body.ingredients) {
    // Create ingredient if needed, then get its id
    ingredientIds = await Promise.all(body.ingredients.map(async ingredient => {
      const foundIngredient = await Ingredient
        .findOneAndUpdate({ name: ingredient },
          { name: ingredient },
          { upsert: true, new: true }
        );
      return foundIngredient.id;
    }));
  } else {
    ingredientIds = [];
  }

  const newMeal = new Meal({
    name: body.name,
    home: body.home,
    time: body.time,
    user: user._id,
    ingredients: ingredientIds
  });

  // Add new meal document
  const savedMeal = await newMeal.save();

  response.json(savedMeal);
});

mealsRouter.delete('/:id', async (request, response) => {

  const user = await helper.authenticateUser(request, response);

  const mealToRemove = await Meal.findById(request.params.id);

  if (!mealToRemove) {
    response.status(400).json({ error: 'Content already deleted' });
  }

  if (!(user._id.toString() === mealToRemove.user.toString())) {
    return response.status(401).json({ error: 'User missing authorisation' });
  }

  await mealToRemove.remove();

  response.status(204).end();

});

/*
WARNING: The following endpoint is only meant for adding test data 
*/
mealsRouter.post('/bulkaddmeals', async (request, response) => {
  const body = request.body
  const user = await helper.authenticateUser(request, response);

  body.forEach(async (item) => {

    let ingredientIds;
    if (item.ingredients) {
      // Create ingredient if needed, then get its id
      ingredientIds = await Promise.all(item.ingredients.map(async ingredient => {
        const foundIngredient = await Ingredient
          .findOneAndUpdate({ name: ingredient },
            { name: ingredient },
            { upsert: true, new: true }
          );
        return foundIngredient.id;
      }));
    } else {
      ingredientIds = [];
    }

    const newMeal = new Meal({
      name: item.name,
      home: item.home,
      time: item.time,
      user: user._id,
      ingredients: ingredientIds
    });

    // Add new meal document
    const savedMeal = await newMeal.save();

    response.json(savedMeal);
  })
});


module.exports = mealsRouter;