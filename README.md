[![pipeline status](https://gitlab.com/moryama/foodpick-be/badges/master/pipeline.svg)](https://gitlab.com/moryama/foodpick-be/-/commits/master)

# FoodPick API

This API is the backend of FoodPick, a single-page application to log and search food ideas.

Look at the [frontend](https://gitlab.com/moryama/foodpick).

Check out the app [demo](https://foodpick-fe-core.onrender.com/) (it's on a free tier, slow loading is expected).

## Content:
- [Features](#features)
- [Tools](#tools)
- [TODOs](#todos)
- [Usage](#usage)
- [Development notes](#dev-notes)
    - [Client - API workflow](#workflow)
    - [Database choice](#database)
    - [MongoDB schema](#db-schema)
    - [CI/CD pipelines implementation](#pipelines)
    - [API endpoints plan](#api-plan)

## Features <a name="features"></a>

- `Node.js`/`Express` server
- REST API
- user token authentication
- middleware functions
- handling endpoint errors
- document-oriented database
- data validation
- unit tests
- CI/CD pipelines

## Tools <a name="tools"></a>

- Node.js 12.18.3
- Express 4.17.1
- [JWT](https://jwt.io/) and [Bcrypt](https://www.npmjs.com/package/bcrypt) to manage authentication
- MongoDB and [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) for database storage
- [mongoose](https://mongoosejs.com/docs/) to manage MongoDB
- [mongoose-unique-validator](https://www.npmjs.com/package/mongoose-unique-validator) for extra data validation
- [Jest](https://jestjs.io/) for automated testing of endpoints
- VS Code [Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) for manual testing of requests
- [launchd](https://betterprogramming.pub/schedule-node-js-scripts-on-your-mac-with-launchd-a7fca82fbf02?gi=b321d3b310e6) to schedule jobs on Mac OS

## TODOs and beyond <a name="todos"></a>

- deploy ✅
- add a CI/CD pipeline ✅
- add tests for `ingredients` endpoint
- optimize Cypress tests: reduce execution time and make it easier to run tests separately
- schedule reset database for demo application ✅
- 🐞  FIXED/BUG: Cypress tests passing locally but not in the pipeline ✅

## Usage <a name="usage"></a>

```
$ git clone https://gitlab.com/moryama/foodpick-be.git
$ cd foodpick-be
$ npm install
```

**Run development server**

```
npm run dev
```

The server will run on [http://localhost:3001](http://localhost:3001).

**Run tests**

```
npm test
```

It is recommended to run tests one file at a time, for example:

```
npm test -- tests/users_api.test.js
```

# Development notes <a name="dev-notes"></a>

## **Client-API workflow** <a name="workflow"></a>
<img src="img/workflow.jpg" alt="client-API workflow" title="client-API workflow" width="700" />


## **Database choice** <a name="database"></a>

Starting the project I decided to take a chance and get familiar with MongoDB.

All was well, until the moment I wanted to implement a filter-by-ingredient functionality.

I realized that I was in fact looking to make a *relation* between `Meal` and `Ingredient` documents. 

In hindsight a relational database, like PostgreSQL, could have been a more suitable choice and I might change the project to use one in the future.

## **MongoDB Schema** <a name="tools"></a>
<img src="img/db_schema.jpg" alt="MongoDB schema" title="MongoDB schema" width="500" />


## **CI/CD pipelines implementation** <a name="pipelines"></a>

As the project was growing, I needed a streamline process to integrate and deploy my application in one command.

I was already using a pipeline made with **`npm` scripts**. Now I wanted to use the **GitLab** pipeline service as it offers more options for flexibility, organization and feedback.

The process of learning and implementing it kept me busy over a couple of weeks. 

I experimented and tested locally as much as possible, but lots of testing was done through actually running the pipelines. I used two different mock versions of the project: one light-weight to play with pipelines structure/interaction and one cloned from the original project to test building, artifacts and general behaviour.

In the end I am satisfied with the result as it answers my current needs for this project and is a good basis to customize for future ones. There is room for improvement, particularly in terms of execution-time and efficiency.

Here are some notes about how I did it.

> The app is currently not deployed anywhere, so the `deploy` step of the pipeline was removed.

### **PROJECT STRUCTURE**

The project is made of two separated repositories, UI and API.

<img src="img/structure.jpg" alt="project-structure" title="Project structure" width="400" />


Moving to implement a GitLab CI/CD pipeline I had these requirements:
- keep the UI and API in their current separated repositories
- do not disrupt the app that was already deployed on Heroku
- include existing unit and e2e tests
- be able to trigger the pipeline with a push from either UI or API

### **APPROACH**

To implement GitLab CI/CD pipelines I considered 2 approaches and I decided for the second.

**As a first approach**, I considered using **subtrees** to unify the UI and API under one common repository while keeping the original separated structure. This would have let me create one deployment pipeline and push the project as a whole.

After learning about and playing with subtrees for a (short) while, this approach seemed a bit tricky, hard to maintain long-term and an overkill for the project. 

**As a second approach**, I considered GitLab [**multiproject pipelines**](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html) which lets you connect pipelines from different repositories in various ways.

After checking this was doable with the GitLab free tier, I chose this approach because it allows me to keep the project structure untouched and easily push to deployment from the UI or the API repository. In addition, GitLab amazing documentation promised great support as usual.

### **IMPLEMENTATION**

This is the double pipeline I implemented (see the backend `.yaml` [file](https://gitlab.com/moryama/foodpick/-/blob/master/.gitlab-ci.yml) and frontend `.yaml` [file](https://gitlab.com/moryama/foodpick/-/blob/master/.gitlab-ci.yml))

<img src="img/pipelines.jpg" alt="project-pipelines" title="Project pipelines" width="700" />

### **FEATURES**

- When pushing to one pipeline, the other one is also triggered. 
- Trigger jobs are skipped whenever not needed by `rules` defined to exclude them.
- The UI `build/` is created in the UI pipeline and shared as an artifact with the API pipeline.
- In the UI pipeline `node_modules` are passed as caches between jobs.

### **CHALLENGES**

I encountered several challenges, as I was quite new to GitLab pipelines and this structure was not so straightforward. 

Here are a couple challenges:

- the initial project setup used a `DATABASE_URI`, passed as a `.env` variable, which included username and password. When I wanted to pass this `URI` as a variable to the pipelines I encountered a problem. This string format cannot be masked using the GitLab UI, because it includes special characters. To solve this I followed the `mongoose` documentation and modified the code to pass `uri`, `user` and `password` as separate variables to the `mongoose.connect()` method. This way I can pass the 3 variables separately in the GitLab UI and mask the password. I like this solution as it makes the source code cleaner and more efficient too.

- I wanted to have the e2e tests run right at the end of the pipeline before deployment. For this reason I had to move the Cypress tests folder from the UI repository, where they lived, to the API repository. This also solved the problem of Cypress needing the API server running in parallel to execute the tests. I added a script in the e2e job to run them both together using the `&` syntax. When first running the tests in the pipeline I also had a problem of missing `Xvfb`, solved by setting this job to use a Cypress `image` which comes with all packages Cypress needs to run effectively.

## **API endpoints plan** <a name="api-plan"></a>

<img src="img/api_plan.jpg" alt="API endpoints plan" title="API endpoints plan" width="800" />
<img src="img/api_plan2.jpg" alt="API endpoints plan 2" title="API endpoints plan 2" width="800" />
