const config = require('./utils/config');

const express = require('express');
require('express-async-errors');
const app = express();

const cors = require('cors');
const mealsRouter = require('./controllers/meals');
const usersRouter = require('./controllers/users');
const loginRouter = require('./controllers/login');
const middleware = require('./utils/middleware');
const morgan = require('morgan');
const logger = require('./utils/logger');
const mongoose = require('mongoose');
const ingredientsRouter = require('./controllers/ingredients');

// Connect to the database
const url = config.MONGODB_URI;

logger.info('Connecting to', url);

mongoose.connect(url, {
  user: config.MONGO_USER,
  pass: config.MONGO_PASS,
})
  .then(() => {
    logger.info('connected to MongoDB');
  })
  .catch((error) => {
    logger.info('error connecting to MongoDB:', error.message);
  });

// Load middleware
app.use(cors());
app.use(express.static('build'));
app.use(express.json());

morgan.token('body', (req) => JSON.stringify(req.body));
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan(':method :url :status :res[content-length] :response-time ms :body'));
}

app.use('/api/login', loginRouter);
app.use('/api/users', usersRouter);
app.use('/api/meals', mealsRouter);
app.use('/api/ingredients', ingredientsRouter);

if (process.env.NODE_ENV === 'test') {
  const testingRouter = require('./controllers/testing');
  app.use('/api/testing', testingRouter);
}

app.use(middleware.unknownEndpoint);
app.use(middleware.errorHandler);

module.exports = app;