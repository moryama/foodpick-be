// The script below is run automatically once a day by a launchd job on Mac OS.

// For details see the related launchd job file:
// /Users/arianna/Library/LaunchAgents/com.arianna.foodpickResetDb.plist

const resetDb = require('./resetDb');


const now = new Date();

const runResetDb = () => {

  process.stderr.write(`${now}\n`);

  try {
    resetDb.resetDemoDatabase();  
  } catch (error) {
    process.stderr.write('The following error occurred while trying to reset the demo database:\n')
    process.stderr.write(`${error}\n`)
    process.exit(1)
  }
  
  process.stdout.write('Successfully reset database.\n');

};

runResetDb();
