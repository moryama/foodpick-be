const app = require('../app'); // Required to let mongoose connect to database
const Meal = require('../models/meal');
const Ingredient = require('../models/ingredient');
const User = require('../models/user');
const constants = require('./constants');


const USERID = constants.USERID;
const MEALS = constants.MEALS;


const getUser = async (userId) => {
  try {
    const user = await User.findById(USERID);
    return user;
  } catch (error) {
    process.stdout.write('The following error happened while trying to retrieve user:');
    process.stdout.write(error);
    process.exit(1);
  }
};

const saveIngredients = async (mealIngredients) => {
  ingredientIds = await Promise.all(mealIngredients.map(async ingredient => {
    const foundIngredient = await Ingredient
      .findOneAndUpdate({ name: ingredient },
        { name: ingredient },
        { upsert: true, new: true }
      );
    return foundIngredient.id;
  }));

  return ingredientIds;
};

const saveMeal = async (newMeal) => {
  try {
    const savedMeal = await newMeal.save();
    return savedMeal;
  } catch (error) {
    process.stdout.write('The following error happened while trying to save a new meal:');
    process.stdout.write(error);
    process.exit(1);
  }
};

const resetDemoDatabase = async () => {

  const user = await getUser(USERID);

  // Clear database
  await Meal.deleteMany({ user: USERID });
  user.meals = [];
  user.ingredients = [];
  
  // Save content to database
  MEALS.map(async meal => {
    const newIngredients = meal.ingredients;

    let ingredientIds;
    if (newIngredients) {
      ingredientIds = await saveIngredients(newIngredients);
    } else {
      ingredientIds = [];
    }

    const newMeal = new Meal({
      name: meal.name,
      home: meal.home,
      time: meal.time,
      user: USERID,
      ingredients: ingredientIds
    });

    const savedMeal = await saveMeal(newMeal);

    // Update user
    user.meals = user.meals.concat(savedMeal._id);
    user.ingredients = user.ingredients.concat(ingredientIds.filter(id =>
      !user.ingredients.includes(id)
    ));

  });

  try {
    await user.save();
    process.exit(0);
  } catch (error) {
    process.stdout.write('The following error happened while trying to save user:');
    process.stdout.write(`${error}`);
    process.exit(1);
  }

};;

module.exports = {
  resetDemoDatabase
};