const DEMOUSERID = '60290b995508841a3eef28e0';
const bubbaUSERID = '60290b905508841a3eef28df';

const USERID = DEMOUSERID;

const MEALS = [
  {
    name: 'pasta with broccoli',
    home: true,
    time: 20,
    ingredients: ['pasta', 'broccoli', 'garlic']
  },
  {
    name: 'English breakfast',
    home: true,
    time: 10,
    ingredients: ['eggs', 'beans', 'bread', 'bacon']
  },
  {
    name: 'pizza from Amunì',
    home: false,
    time: 30
  },
  {
    name: 'vegetarian carbonara',
    home: true,
    time: 30,
    ingredients: ['eggs', 'pasta', 'zucchini', 'onion']
  },
  {
    name: 'home made burger',
    home: true,
    time: 45,
    ingredients: ['beef', 'bread', 'tomato', 'onion', 'mayo']
  },
  {
    name: 'carbonara',
    home: true,
    time: 20,
    ingredients: ['pasta', 'bacon', 'eggs', 'onion', 'parmisan']
  },
  {
    name: 'burger from Kantýna',
    home: false,
    time: 60
  }
];


module.exports = {
  USERID,
  MEALS
};