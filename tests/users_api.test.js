const mongoose = require('mongoose');
const supertest = require('supertest');
const app = require('../app');
const User = require('../models/user');
const helper = require('./test_helper');
const auth_helper = require('../utils/auth_helper');
const api = supertest(app);

beforeEach(async () => {
  await User.deleteMany();
  const userObjects = helper.initialUsers.map(user => new User(user));
  const promiseToSaveEachUser = userObjects.map(user => user.save());
  await Promise.all(promiseToSaveEachUser);
});

describe('when some users are already saved', () => {
  test('users are returned as json', async (done) => {
    await api
      .get('/api/users')
      .expect(200)
      .expect('Content-Type', /application\/json/);
    done();
  });

  test('user password is not returned', async () => {
    const response = await api
      .get('/api/users');
    expect(response.body[0]).not.toMatchObject({ password: 'password' });
  });

  test('all users are returned', async () => {
    const usersInDb = await helper.usersInDb();
    const response = await api.get('/api/users');

    expect(response.body).toHaveLength(usersInDb.length);
  });

  test('a specific user is within the returned users', async () => {
    const response = await api.get('/api/users');

    const usernames = response.body.map(user => user.username);
    expect(usernames).toContain('other user');
  });

  test('returns username of a specific user', async () => {
    const existingUsers = await helper.usersInDb();
    const userToGet = existingUsers[0];

    const response = await api
      .get(`/api/users/${userToGet.id}`)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    expect(response.body.username).toBe(userToGet.username);
  });
});

describe('addition of a new user', () => {
  test('a user can be added', async () => {
    const usersAtStart = await helper.usersInDb();

    const newUser = {
      username: 'new user',
      password: 'password'
    };

    await api
      .post('/api/users')
      .send(newUser)
      .expect(201)
      .expect('Content-Type', /application\/json/);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length + 1);
  });

  test('user is not added and returns appropriate status code and message when user has no username', async () => {
    const usersAtStart = await helper.usersInDb();

    const newUser = {
      password: 'password'
    };

    const response = await api
      .post('/api/users')
      .send(newUser)
      .expect(400);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);

    expect(response.body.error).toContain('Username or password missing');
  });

  test('user is not added and returns appropriate status code and message when password is less than minimum length', async () => {
    const usersAtStart = await helper.usersInDb();

    const newUser = {
      username: 'new user',
      password: 'pa'
    };

    const response = await api
      .post('/api/users')
      .send(newUser)
      .expect(400);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);

    const minLength = auth_helper.PASSWORD_MIN_LENGTH;
    expect(response.body.error).toContain(
      `Password must be minimum ${minLength} characters`
    );
  });

  test('user is not added and returns appropriate status code and message when username is not unique', async () => {
    const usersAtStart = await helper.usersInDb();

    const newUser = {
      username: 'user',
      password: 'password'
    };

    const response = await api
      .post('/api/users')
      .send(newUser)
      .expect(400);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);

    expect(response.body.error).toContain(
      'expected `username` to be unique'
    );
  });
});

describe('deletion of a user', () => {
  test('succeeds with status code 204 when id is valid', async () => {
    const usersAtStart = await helper.usersInDb();
    const userToDelete = usersAtStart[0];

    await api
      .delete(`/api/users/${userToDelete.id}`)
      .expect(204);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length - 1);
    expect(usersAtEnd).not.toContain(userToDelete);
  });

  test('fails with status code 400 when id is not valid', async () => {
    const usersAtStart = await helper.usersInDb();
    const validnonExistingUserId = await helper.nonExistingUserId;

    await api
      .delete(`/api/users/${validnonExistingUserId}`)
      .expect(400);

    const usersAtEnd = await helper.usersInDb();
    expect(usersAtEnd).toHaveLength(usersAtStart.length);
  });
});


afterAll(() => {
  mongoose.connection.close();
});