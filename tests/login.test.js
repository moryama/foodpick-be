const mongoose = require('mongoose');
const supertest = require('supertest');
const bcrypt = require('bcrypt');
const app = require('../app');
const User = require('../models/user');
const helper = require('./test_helper');

const api = supertest(app);

beforeEach(async () => {
  await User.deleteMany();

  const initialUsers = helper.initialUsers;
  const hashedPasswords = initialUsers.map(u => bcrypt.hash(u.password, 10));
  const promisedHashedPasswords = await Promise.all(hashedPasswords);
  initialUsers.map(user => {
    user.passwordHash = promisedHashedPasswords[initialUsers.indexOf(user)];
  });
  const userObjects = initialUsers.map(user => new User(user));
  const promiseToSaveEachUser = userObjects.map(user => user.save());
  await Promise.all(promiseToSaveEachUser);
});

describe('user login', () => {
  test('succeeds when user exists', async (done) => {
    const existingUser = helper.initialUsers[0];

    await api
      .post('/api/login')
      .send(existingUser)
      .expect(200)
      .expect('Content-Type', /application\/json/);
    done();
  });

  test('fails with status code 401 and appropriate message if username does not exist', async (done) => {
    const nonExistingUser = {
      username: 'not a user',
      password: 'password'
    };

    const response = await api
      .post('/api/login')
      .send(nonExistingUser)
      .expect(401);
    done();

    expect(response.body.error).toContain('Invalid username');
  });

  test('fails with status code 401 and appropriate message if password is incorrect', async (done) => {
    const nonExistingUser = {
      username: 'user',
      password: 'wrong password'
    };

    const response = await api
      .post('/api/login')
      .send(nonExistingUser)
      .expect(401);
    done();

    expect(response.body.error).toContain('Invalid password');
  });

  test('fails with status code 400 and appropriate message if password is missing', async (done) => {
    const response = await api
      .post('/api/login')
      .send({ username: 'user' })
      .expect(400);
    done();

    expect(response.body.error).toContain('Password is missing');
  });

  test('fails with status code 400 and appropriate message if password is missing', async (done) => {
    const response = await api
      .post('/api/login')
      .send({ password: 'password' })
      .expect(400);
    done();

    expect(response.body.error).toContain('Username is missing');
  });

});

afterAll(() => {
  mongoose.connection.close();
});