const Meal = require('../models/meal');
const User = require('../models/user');

const initialUsers = [
  {
    username: 'user',
    password: 'password',
  },
  {
    username: 'other user',
    password: 'password'
  }
];

const initialMeals = [
  {
    name: 'panino di Kantyna',
    home: false,
    time: 0.5,
  },
  {
    name: 'zucca al forno',
    home: true,
    time: 1
  },
  {
    name: 'tortelli di zucca',
    home: true,
    time: 3
  }
];

const mealsInDb = async () => {
  const meals = await Meal.find({});
  return meals.map(meal => meal.toJSON());
};

const usersInDb = async () => {
  const users = await User.find({});
  return users.map(user => user.toJSON());
};

const nonExistingUserId = async () => {
  const user = new User({ username: 'willremovethissoon' });
  await user.save();
  await user.remove();

  return user._id.toString();
};

const nonExistingMealId = async () => {
  const meal = new Meal({ name: 'willremovethissoon' });
  await meal.save();
  await meal.remove();

  return meal._id.toString();
};


module.exports = {
  initialMeals,
  initialUsers,
  mealsInDb,
  nonExistingMealId,
  nonExistingUserId,
  usersInDb
};