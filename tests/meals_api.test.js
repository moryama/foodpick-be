const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const supertest = require('supertest');
const mongoose = require('mongoose');
const helper = require('./test_helper');
const app = require('../app');
const api = supertest(app);

const Meal = require('../models/meal');
const User = require('../models/user');

let authHeaders = null;

beforeEach(async () => {
  await Meal.deleteMany({});
  await User.deleteMany({});

  // Create a user
  const passwordHash = await bcrypt.hash('password', 10);
  const newUser = new User({
    username: 'new user',
    passwordHash
  });
  const user = await newUser.save();

  // Create authentication headers
  const userForToken = {
    username: user.username,
    id: user._id
  };
  const token = jwt.sign(userForToken, process.env.SECRET);
  authHeaders = `bearer ${token}`;

  // Create some ingredients
  // TODO

  // Assign ingredients to meals
  // TODO

  // Populate the database with meals
  const initialMeals = helper.initialMeals;
  initialMeals.forEach(meal => meal.user = user._id);
  const mealObjects = initialMeals.map(meal => new Meal(meal));
  const promiseToSaveEachMealArray = mealObjects.map(meal => meal.save());
  await Promise.all(promiseToSaveEachMealArray);
});


describe('when some meals are already saved', () => {
  test('meals are returned as json', async () => {
    await api
      .get('/api/meals')
      .set('Authorization', authHeaders)
      .expect(200)
      .expect('Content-Type', /application\/json/);
  });

  test('only the user\'s meals are returned', async () => {
    // Create another user with a meal
    const passwordHash = await bcrypt.hash('password', 10);
    const anotherUser = new User({
      username: 'another user',
      passwordHash
    });
    await anotherUser.save();
    const notMyMeal = new Meal({
      name: 'not my meal',
      user: anotherUser._id
    });
    notMyMeal.save();

    const response = await api
      .get('/api/meals')
      .set('Authorization', authHeaders)
      .expect(200);

    const mealsInDb = await helper.mealsInDb();
    expect(response.body).toHaveLength(mealsInDb.length - 1);
    expect(response.body).not.toContain(notMyMeal);
  });

  test('all meals are returned', async () => {
    const mealsInDb = await helper.mealsInDb();
    const response = await api
      .get('/api/meals')
      .set('Authorization', authHeaders);

    expect(response.body).toHaveLength(mealsInDb.length);
  });

  test('a specific meal is within the returned meals', async () => {
    const response = await api
      .get('/api/meals')
      .set('Authorization', authHeaders);

    const names = response.body.map(r => r.name);
    expect(names).toContain('tortelli di zucca');
  });

  describe('when accessing the meals ingredients', () => {
    test('all related ingredients are returned', async () => {
      // TODO
    });

    test('a specific ingredients is among returned ingredients', async () => {
      // TODO
    });

    test('ingredients have no duplicates', async () => {
      // TODO
    });
  });

});

describe('addition of a new meal', () => {
  test('a meal can be added', async () => {
    const mealsAtStart = await helper.mealsInDb();
    const newMeal = {
      name: 'pasta al pomodoro',
      home: true,
      time: 0.5
    };

    await api
      .post('/api/meals')
      .send(newMeal)
      .set('Authorization', authHeaders)
      .expect(200)
      .expect('Content-Type', /application\/json/);

    const MealsAtEnd = await helper.mealsInDb();
    expect(MealsAtEnd).toHaveLength(mealsAtStart.length + 1);

    const names = MealsAtEnd.map(meal => meal.name);
    expect(names).toContain(
      'pasta al pomodoro'
    );
  });

  test.skip('fails with status code 401 and error message if user is not authenticated', async () => {
    const newMeal = {
      name: 'pasta al pomodoro',
      home: true,
      time: 0.5
    };

    const response = await api
      .post('/api/meals')
      .send(newMeal)
      .expect(401);

    expect(response.body.error).not.toBe(undefined);

    const MealsAtEnd = await helper.mealsInDb();
    expect(MealsAtEnd).toHaveLength(helper.initialMeals.length);
  });

  test('a meal without a name is not added', async () => {
    const newMeal = {
      home: true,
      time: 1
    };

    await api
      .post('/api/meals')
      .send(newMeal)
      .set('Authorization', authHeaders)
      .expect(400);

    const MealsAtEnd = await helper.mealsInDb();
    expect(MealsAtEnd).toHaveLength(helper.initialMeals.length);
  });

  test('a meal without ingredients is added with an empty list of ingredients', async () => {
    const newMeal = {
      name: 'new meal'
    };

    await api
      .post('/api/meals')
      .send(newMeal)
      .set('Authorization', authHeaders)
      .expect(200);

    const MealsAtEnd = await helper.mealsInDb();
    expect(MealsAtEnd).toHaveLength(helper.initialMeals.length + 1);

    const addedMeal = MealsAtEnd.find(meal => meal.name === newMeal.name);
    expect(addedMeal.ingredients).toEqual([]);
  });

});

describe('deletion of a meal', () => {
  test('succeeds with status code 204 when id is valid and user is authenticated', async () => {
    const mealsAtStart = await helper.mealsInDb();
    const mealToDelete = mealsAtStart[0];

    await api
      .delete(`/api/meals/${mealToDelete.id}`)
      .set('Authorization', authHeaders)
      .expect(204);

    const mealsAtEnd = await helper.mealsInDb();
    expect(mealsAtEnd).toHaveLength(mealsAtStart.length - 1);

    const mealNames = mealsAtEnd.map(meal => meal.name);
    expect(mealNames).not.toContain(mealToDelete.name);
  });

  test('deletion fails with status code 401 and error message if user is not authenticated', async () => {
    const mealsAtStart = await helper.mealsInDb();
    const mealToDelete = mealsAtStart[0];

    const response = await api
      .delete(`/api/meals/${mealToDelete.id}`)
      .expect(401);

    expect(response.body.error).not.toBe(undefined);

    const mealsAtEnd = await helper.mealsInDb();
    expect(mealsAtEnd).toHaveLength(mealsAtStart.length);
  });

  test('fails with status code 400 when meal id is not valid', async () => {
    const mealsAtStart = await helper.mealsInDb();
    const validNonExistingMealId = await helper.nonExistingMealId();

    await api
      .delete(`/api/meals/${validNonExistingMealId}`)
      .set('Authorization', authHeaders)
      .expect(400);

    const mealsAtEnd = await helper.mealsInDb();
    expect(mealsAtEnd).toHaveLength(mealsAtStart.length);
  });
});

afterAll(() => {
  mongoose.connection.close();
});