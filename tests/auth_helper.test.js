const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const supertest = require('supertest');
const mongoose = require('mongoose');
const helper = require('./test_helper');
const app = require('../app');
const api = supertest(app);

const Meal = require('../models/meal');
const User = require('../models/user');

let authHeaders = null;

beforeEach(async () => {
  await Meal.deleteMany({});
  await User.deleteMany({});

  // Create a user
  const passwordHash = await bcrypt.hash('password', 10);
  const newUser = new User({
    username: 'new user',
    passwordHash
  });
  const user = await newUser.save();

  // Create authentication headers
  const userForToken = {
    username: user.username,
    id: user._id
  };
  const token = jwt.sign(userForToken, process.env.SECRET);
  authHeaders = `bearer ${token}`;

  // Populate the database with meals
  const initialMeals = helper.initialMeals;
  initialMeals.forEach(meal => meal.user = user._id);
  const mealObjects = initialMeals.map(meal => new Meal(meal));
  const promiseToSaveEachMealArray = mealObjects.map(meal => meal.save());
  await Promise.all(promiseToSaveEachMealArray);
});

describe('when authenticating a user', () => {

  test('fails with status code 401 and appropriate message when authentication token is missing', async () => {
    const response = await api
      .get('/api/meals')
      .expect(401);

    expect(response.body.error).toContain('token is missing');
  });

  test('fails with status code 401 and appropriate message when user is non-existing', async () => {

    const nonExistingUserId = await helper.nonExistingUserId();
    const userForToken = {
      username: 'nonExistingUser',
      id: nonExistingUserId
    };
    const nonExistingToken = jwt.sign(userForToken, process.env.SECRET);
    authHeaders = `bearer ${nonExistingToken}`;

    const response = await api
      .get('/api/meals')
      .set('Authorization', authHeaders)
      .expect(401);

    expect(response.body.error).toContain('invalid user');
  });
});


afterAll(() => {
  mongoose.connection.close();
});